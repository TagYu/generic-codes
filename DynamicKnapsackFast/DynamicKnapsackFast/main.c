//
//  main.c
//  DynamicKnapsack
//
//  Created by Yuki Taguchi on 2014/12/27.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ADD 1
#define NOT_ADD 0
#define NOT_EXIST -1

typedef struct listent {
    int item;
    struct listent *next;
} ListEntry;

typedef struct itemset {
    int flag;
    int weightsum;
} ItemSet;

void addSet(ItemSet *s, int i, int weight);
int  sumSet(ItemSet *s, int v[]);
void copySet(ItemSet *s1, ItemSet *s2);
void printSet(ItemSet **T, int i, int j, int *price_list);
void recursiveFree(ListEntry *s);
void printAnswer(ItemSet **T,int n, int price_sum, int *price_list);
int min(int a, int b);

int main(int argc, char *argv[])
{
    FILE *file;
    int n, bag_size, price_sum;
    int i, j;

    clock_t start, end;
    start = clock();

    /* ファイルからの情報読み込み
     第一引数に与えたファイル名を入力として読み込む */
    if ((file = fopen(argv[1],"r")) == NULL){
        printf("unable to open the file!\n");
        exit(1);
    }
    
    /* アイテム数，袋容量の読み込み */
    fscanf(file, "%d,%d\n", &n, &bag_size);
    
    int weight_list[n];
    int price_list[n];
    
    char buf[n * 15];
    char *tp;
    
    /* 各アイテム重みの読み込み */
    j = 0;
    fgets(buf, n * 15, file);
    tp = strtok(buf, ",");
    while (tp != NULL && j < n) {
        weight_list[j] = atoi(tp);
        j++;
        tp = strtok(NULL, ",");
    }
    
    /* 各アイテム価値の読み込み */
    j = 0;
    price_sum = 0;
    fgets(buf, n * 15, file);
    tp = strtok(buf, ",");
    while (tp != NULL && j < n) {
        price_list[j] = atoi(tp);
        price_sum = price_sum + price_list[j];
        j++;
        tp = strtok(NULL, ",");
    };
    /* ItemSetの配列Tを初期化 */
    ItemSet *T[n];
    for (i = 0; i < n; i++){
        T[i] = (ItemSet *)malloc((price_sum +1) * sizeof(ItemSet));
        for (j = 0; j < price_sum + 1; j++){
            T[i][j].flag = NOT_EXIST;
            T[i][j].weightsum = 0;
        }
    };
    /* ファイルからの情報読み込みここまで */

    /* i=0 の段の計算 */
    if (weight_list[0] <= bag_size){
        T[0][price_list[0]].flag = ADD;
        T[0][price_list[0]].weightsum += weight_list[0];
    }
    T[0][0].flag = NOT_ADD;
    T[0][0].weightsum = 0;
    
    /* DPによるi > 0 の部分の表計算 */
    int k;
    for (i = 1; i < n; i++)  {
        for (k = 0; k <= price_sum; k++) {
            /* 重さをコピー */
            T[i][k].weightsum = T[i-1][k].weightsum;
            /* flagを設定 */
            if (T[i-1][k].flag == NOT_EXIST) {
                T[i][k].flag = NOT_EXIST;
            } else {
                T[i][k].flag = NOT_ADD;
            }
            /* 添え字が負数でないかチェック */
            if ((k - price_list[i]) < 0) continue;
            /* T[i][k]が存在するかどうかチェック */
            if (T[i][k].flag == NOT_EXIST &&
                T[i-1][k - price_list[i]].flag != NOT_EXIST &&
                T[i-1][k - price_list[i]].weightsum + weight_list[i] <= bag_size) {
                    T[i][k].flag = ADD;
                    T[i][k].weightsum = T[i-1][k - price_list[i]].weightsum + weight_list[i];
            } else if (T[i][k].flag != NOT_EXIST &&
                T[i-1][k - price_list[i]].flag != NOT_EXIST &&
                T[i-1][k - price_list[i]].weightsum + weight_list[i] <= min(bag_size, T[i][k].weightsum)) {
                    T[i][k].flag = ADD;
                    T[i][k].weightsum = T[i-1][k - price_list[i]].weightsum + weight_list[i];
            }
        }
    }

    end = clock();
    char *t_path = "/Users/yuki/Dropbox/Document/基礎アルゴリズム/exe4/result_knap_fast.txt";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- %s ---------------\n", argv[1]);
    fprintf(t_fp, "開始時刻:%d\n", start );
    fprintf(t_fp, "終了時刻:%d\n", end );
    fprintf(t_fp, "処理時間:%d[ms]\n", (end - start) / 1000 );
    fclose(t_fp);

//    for (i = 0; i < n; i++) {
//        for (j = 0; j < price_sum; j++) {
//            printf("T[%d][%d].flag = %d\n", i, j, T[i][j].flag);
//
//        }
//    }
    printAnswer(T, n, price_sum, price_list);

}

void printSet(ItemSet **T, int i, int j, int *price_list)
{
    if (i < 0 || j < 0) return;
    if (T[i][j].flag == ADD) {
        printf("%d,", i);
        printSet(T, i-1, j - price_list[i], price_list);
    } else if (T[i][j].flag == NOT_ADD) {
        printSet(T, i-1, j, price_list);
    } else if (T[i][j].flag == NOT_EXIST) {
        
    }
    printf("\n");
}


void recursiveFree(ListEntry *s) {
    
    ListEntry *p;
    
    if (s != NULL) {
        p = s->next;
        free(s);
        recursiveFree(p);
    }
    return;
}

void printAnswer(ItemSet **T,int n, int price_sum, int *price_list) {
    int i = 0, j = 0, breakFlag = 0;
    for (i = n - 1; i >= 0; i--) {
        for (j = price_sum - 1; j >= 0; j--) {
            if (T[i][j].flag != NOT_EXIST) {
                breakFlag = 1;
                break;
            }
        }
        if (breakFlag) break;
    }
    printf("答えはT[%d][%d]のとき", i,j);
    printSet(T, i , j, price_list);
}

int min(int a, int b) {
    return (b < a) ? b : a;
}

/*
 * メモスペース
 */

/* この時点で，各配列，変数には以下のように情報が格納されている

 n : アイテムの総数
 bag_size : 袋の容量
 price_sum : 全アイテム価値の合計値
 weight_list[i] : アイテム i の重さ
 price_list[i] : アイテム i の価値

 アイテムの添字は0～n-1の範囲とする．

 また，配列T[i][j]はアイテム[0, i]までを使った，価値jの解を
 格納する配列である．T[i][j] は プログラム冒頭で定義されている
 ItemSet型の配列である．構造体ItemSetの各エントリはそれぞれ
 以下のような意味を持つ

 int flag : 集合が存在するかどうかを表すフラグ．もし集合が存在しない
 ときはflag = NOT_EXISTが代入されている．
 ListEntry *set : 集合に含まれるアイテムの添字を記録するリスト構造

 NOT_EXISTは空集合を意味するわけではないことに注意せよ．

 ItemSet型のエントリを操作するために，以下の関数が定義されている．

 void addSet(ItemSet *s, int i);
 s に アイテム iを追加する．追加した時点で，s.flagは
 　 値がかならずEXISTに変わる．iとしてEMPTYSETを指定したときは
 空集合が代入される．

 使用例
 addSet(&(T[i][j]), 3)
 アイテム集合T[i][j]にアイテム3を追加．

 int  sumSet(ItemSet *s, int v[]);
 s = {i_1, i_2, ..., i_k} のとき，v[i_1] + v[i_2] + ... + v[i_k]を
 計算する．

 使用例
 sumSet(&(T[i][j]), weight_list)
 アイテム集合T[i][j]の重みの総和を計算

 void copySet(ItemSet *s1, ItemSet *s2);
 集合s1 を s2にコピーする．s2は上書きされる．

 使用例
 copySet(&(T[i][j]), &(T[k][h]))
 集合T[i][j]の中身をT[k][h]にコピーする

 void printSet(ItemSet *s);
 集合sの中身をカンマ区切りで出力する．

 使用例
 printSet(&(T[i][j]));

 上記の関数はいずれもItemSet型を引数で渡すときには
 参照渡し(ポインタ渡し)となることに注意せよ．


 < 課題3 へのヒント >

 課題3は，各ループ時における重みの計算を省略するために，
 直前のループの重みを記録しておく必要がある．なので，
 構造体 ItemSetを以下のように拡張すれば良さそうである．

 typedef struct itemset {
 int flag;
 ListEntry *set;
 int weightsum; (ここで重みの総和を記録)
 } ItemSet;

 しかしながら，これだけではアルゴリズムはO(n^2 max(c_k))時間に
 ならない．なぜならばT[i - i][k] から T[i][k]へのコピー
 (配布スライドで示した擬似コードの5行目)は，一般には
 定数時間では終わらないためである．
 このコピーを定数時間で終わらせる一つの方法は，コピーでなく
 差分を記録することである．実際，アルゴリズムの各ステップの
 動作から，T[i][k]には記録されうる値は，
 (1) T[i-1][k]と同じ，
 (2) T[i-1][k - c_i]にc_iを加えたもの，
 のどちらかのみであるので，ここのいずれを記録したか
 だけを記録しておけば十分であることになる．よって，
 例えば以下のように定義し

 #define ADD 1
 #define NOT_ADD 0
 #define NOT_EXIST -1

 typedef struct itemset {
 int flag;
 int weightsum;
 } ItemSet:

 　T[i][*].flag = NOT_ADDのとき，アイテムiが追加されない((1)のケース)
 T[i][*].flag = ADD のとき アイテムiが追加された((2)のケース)

 のようにすればよい．ただし，このときTの各エントリは
 アイテム集合そのものを保持しないため，printSetの内部等で
 解の全体像を取得するときは少し工夫が必要である．
 */
