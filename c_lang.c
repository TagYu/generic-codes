
/* -------- main()と引数チェック -------- */
#include <stdio.h>
#include <stdlib.h>

static int ARG_NUM = 2; /* 引数の最大数 + 1 */
int main(int argc, char *argv[])
{
    if (argc < ARG_NUM) {
        fprintf(stderr, "Usage: %s [file]\n", argv[0]);
        exit(1);
    }
}


/* ------- ファイルオープン関数 ------- */
FILE* open_file(int argc, char *argv[]) {
    if (argv[1] == NULL) {
        printf("%s\n", "Error: No argument!");
        return NULL;
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("%s\n", "Error: Cannot open file!");
        return NULL;
    }
    return fp;
}


/* -------- 時間計測（結果はファイル保存） -------- */
    clock_t start, end;
    start = clock();
    /* 計測したい処理 */
    end = clock();
    char *t_path = "~/distination_dir/result.txt";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- %s ---------------\n", argv[1]);
    fprintf(t_fp, "開始時刻:%d\n", start );
    fprintf(t_fp, "終了時刻:%d\n", end );
    fprintf(t_fp, "処理時間:%d[ms]\n", (end - start) / 1000 );
    fclose(t_fp);
