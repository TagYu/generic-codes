/*
* File:
* Author: 
*
*   !!!使い方!!!
*   ●MODE_SELECTORを0に設定すると分割統治法、1に設定するとBrute Force
*   ●ファイルはコマンドライン引数で渡すと動きます
*
* Created on 2014/12/05, 8:55
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <limits.h>

#define C 2
#define MODE_SELECTOR 0
typedef struct {
	int x;
	int y;
} Point;

/* ファイルオープン関数 */
FILE* open_file(int argc, char *argv[]) {
    if (argv[1] == NULL) {
        printf("%s\n", "Error: No argument!");
        return NULL;
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("%s\n", "Error: Cannot open file!");
        return NULL;
    }
    return fp;
}

/**
 *  Point構造体のx座標を比較してp1の方が大きければ1を小さければ-1を返す
 *  x座標の値が同じならy座標の差を返す
 *
 *  @param p1 1つ目のPoint
 *  @param p2 2つ目のPoint
 *
 *  @return p1のx座標の方が大きければ1、小さければ-1。値が同じならy座標の差(p1 - p2)
 */
int compare_x(const void *p1, const void *p2) {
	if (((Point *)p1)->x > ((Point *)p2)->x) {
		return 1;
	}
	else if (((Point *)p1)->x < ((Point *)p2)->x){
		return -1;
	}
	else {
		return ((Point *)p1)->y - ((Point *)p2)->y;
	}
}

/**
 *  Point構造体のy座標を比較してp1の方が大きければ1を、小さければ-1を返す
 *  y座標の値が同じならx座標の差を返す
 *
 *  @param p1 1つ目のPoint
 *  @param p2 2つ目のPoint
 *
 *  @return p1のy座標の方が大きければ1、小さければ-1。値が同じならx座標の差(p1 - p2)
 */
int compare_y(const void *p1, const void *p2) {
    if (((Point *)p1)->y > ((Point *)p2)->y) {
        return 1;
    }
    else if (((Point *)p1)->y < ((Point *)p2)->y){
        return -1;
    }
    else {
        return ((Point *)p1)->x - ((Point *)p2)->x;
    }
}

/**
 *  2つのPointerの距離を求める
 *
 *  @param p1
 *  @param p2
 *
 *  @return 平方根をとるまえの距離をlong long intで返す
 */
long long int quad_distance(Point p1, Point p2) {
    return (long long int)(p1.x - p2.x) * (long long int)(p1.x - p2.x) + (long long int)(p1.y - p2.y) * (long long int)(p1.y - p2.y);
}


void bf_closest_pair(Point *list, int n, Point *p1, Point *p2, long long int *qdist) {
	int i, j;
	long long int d;

	*qdist = LONG_MAX;
	for (i = 0; i < n-1; ++i) {
		for (j = i + 1; j < n; ++j) {
			d = quad_distance(list[i], list[j]);
			if (*qdist > d) {
				*p1 = list[i]; *p2 = list[j];
				*qdist = d;
			}
		}
	}
}

void dac_closest_pair_rec(Point *px, Point *py, int n, Point *p1, Point *p2, long long int *qdist) {
    /* 配列の長さがC以下ならBrute Forceで求める */
    if (n <= C) {
        bf_closest_pair(px, n, p1, p2, qdist);
        return;
    }
    Point *plx = (Point *)calloc(n/2, sizeof(Point));
    Point *prx = (Point *)calloc(n - n/2, sizeof(Point));
    Point *ply = (Point *)calloc(n/2, sizeof(Point));
    Point *pry = (Point *)calloc(n - n/2, sizeof(Point));
    /* pxを分割してplxとprxを生成 */
    int i;
    int cntl, cntr;
    cntl = cntr = 0;
    for (i = 0; i < n; i++) {
        if (i < n/2) {
            plx[cntl] = px[i];
            cntl++;
        } else {
            prx[cntr] = px[i];
            cntr++;
        }
    }
    /* pyを振り分けてplyとpryを生成 */
    cntl = cntr = 0;
    for (i = 0; i < n; i++) {
        int flg = compare_x(&py[i], &plx[n/2 - 1]);
        if (flg <= 0) {
            ply[cntl] = py[i];
            cntl++;
        } else {
            pry[cntr] = py[i];
            cntr++;
        }
    }

    /* 再帰的に各ブロックの最短距離を求める */
    Point p1_l, p2_l;
    Point p1_r, p2_r;
    long long int e1, e2;
    dac_closest_pair_rec(plx, ply, n/2, &p1_l, &p2_l, &e1);
    dac_closest_pair_rec(prx, pry, n - n/2, &p1_r, &p2_r, &e2);

    /* 右と左の最短距離を比較 */
    long long int e;
    if (e1 < e2) {
        e = e1;
        *p1 = p1_l;
        *p2 = p2_l;
    } else {
        e = e2;
        *p1 = p1_r;
        *p2 = p2_r;
    }

    /* 分割線の位置を求める */
    int b = px[n/2].x;
    /* 境界線付近のPointを列挙 */
    int cnt = 0;    /* カウント用 */
    Point *sy = (Point *)calloc(n, sizeof(Point));
    for (i = 0; i < n; i++) {
        if (py[i].x > b-sqrt(e) && py[i].x < b+sqrt(e)) {
            sy[cnt] = py[i];
            cnt++;
        }
    }
    /* 左右の境界線をまたいだ最短ペアを探す */
    *qdist = e;    /* 初期化 */
    if (cnt >= 2) {
        for (i = 0; i < cnt - 1; i++) {
            int j = i + 1;
            if (sy[j].y - sy[i].y < e) {
                if (quad_distance(sy[j], sy[i]) < *qdist) {
                    *qdist = quad_distance(sy[j], sy[i]);
                    p1->x = sy[j].x;    p1->y = sy[j].y;
                    p2->x = sy[i].x;    p2->y = sy[i].y;
                }
            }
        }
    }
    return;
}

void dac_closest_pair(Point *p, int n, Point *p1, Point *p2, long long int *qdist)  {

    /* 配列pをx,yそれぞれでソートしたものを作成 */
    Point *px = (Point *)calloc(n, sizeof(Point));
    Point *py = (Point *)calloc(n, sizeof(Point));
    memcpy(px, p, sizeof(Point) * n);
    memcpy(py, p, sizeof(Point) * n);
    qsort(px, n, sizeof(Point), compare_x);
    qsort(py, n, sizeof(Point), compare_y);
    /* 再帰関数を実行 */
    dac_closest_pair_rec(px, py, n, p1, p2, qdist);
    return;
}

/**
 *  main関数
 *
 *  @return 成功かどうか
 */
int main(int argc, char** argv) {

	int n;
	int i;
	Point a, b;
	long long int d;

    FILE *fp = open_file(argc, argv);

	fscanf(fp, "%d", &n);

	Point *plist = (Point *)calloc(n, sizeof(Point));
	if (plist == NULL) {
		printf("memory allocation error.");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < n; ++i){
		fscanf(fp, "%d %d", &(plist[i].x), &(plist[i].y));
	}

    clock_t start, end;
    start = clock();
    
    if (MODE_SELECTOR) {
        bf_closest_pair(plist, n, &a, &b, &d);

    } else {
        dac_closest_pair(plist, n, &a, &b, &d);
    }

    printf("%d,%d\n", a.x, a.y);
    printf("%d,%d\n", b.x, b.y);
	printf("dist=%lld\n", d);
    
    end = clock();
    char *t_path = "/Users/yuki/Dropbox/Document/基礎アルゴリズム/result_dac.txt";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- %s ---------------\n", argv[1]);
    fprintf(t_fp, "開始時刻:%d\n", start );
    fprintf(t_fp, "終了時刻:%d\n", end );
    fprintf(t_fp, "処理時間:%d[ms]\n", end - start );
    fclose(t_fp);
    
    fclose(fp);

	return (EXIT_SUCCESS);
}

