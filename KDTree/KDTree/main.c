//
//  main.c
//  KDTree
//
//  Created by Yuki Taguchi on 2015/01/31.
//  Copyright (c) 2015年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SELECTOR 1

/* 取り扱うデータ数の最大値 */
#define Datasize 100000

/* rの点pがクエリ上に無かったときのステータス */
typedef enum status{
    LEFT,
    RIGHT,
    BELOW,
    ABOVE,
    OVER
} STATUS;

typedef struct point {
    int x;
    int y;
} Point;

typedef struct treenode {
    Point p;
    struct treenode *left;
    struct treenode *right;
} Treenode;

void sortxy(Point *g, int start, int end, int stype);
void rect_search_simple(int x1, int y1, int x2, int y2, Point plist[], int size);
void rect_search(Treenode *r, int x1, int y1, int x2, int y2, int stype);
Treenode *construct_kdtree(Point *g, int start, int end, int stype);


int main(int argc, char *argv[])
{
    FILE *file;
    int x, y;
    int x1, y1, x2, y2;
    int j, size;
    Point plist[Datasize];
    Treenode *root;

    /* ファイルからの情報読み込み
     第一引数に与えたファイル名を入力として読み込む */
    if ((file = fopen(argv[1],"r")) == NULL){
        printf("unable to open the file!\n");
        exit(1);
    }

    /* 計測開始 */
    clock_t start, end;
    start = clock();

    j = 0; size = 0;
    while (fscanf(file,"%d,%d",&x,&y) != EOF) {
        plist[j].x = x;
        plist[j].y = y;
        j++; size ++;
    };

    /* KD木を作る */
    if (SELECTOR) {
        root = construct_kdtree(plist, 0, size - 1, 1);
    }

    fclose(file);

//    getchar();

    /* 第２引数のファイル名をクエリとして読み出す */
    if ((file = fopen(argv[2],"r")) == NULL){
        printf("unable to open the file!\n");
        exit(1);
    }

    j = 0;
    while (fscanf(file,"%d,%d,%d,%d",&x1,&y1,&x2,&y2) != EOF) {
        printf("query = (%d, %d) - (%d, %d)\n", x1, y1, x2, y2);
        /* 問い合わせ (x1, y1) - (x2, y2) の範囲 */
        if (SELECTOR) {
            rect_search(root, x1, y1, x2, y2, 1);
        } else {
            rect_search_simple(x1, y1, x2, y2, plist, size);
        }
            printf("\n");
        j++;
    };

    end = clock();
    char *t_path = "/Users/yuki/Dropbox/Document/基礎アルゴリズム/exe6/result_simple.txt";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- %s ---------------\n", argv[1]);
    fprintf(t_fp, "開始時刻:%d\n", start );
    fprintf(t_fp, "終了時刻:%d\n", end );
    fprintf(t_fp, "処理時間:%d[ms]\n", (end - start) / 1000 );
    fclose(t_fp);

    fclose(file);
    
    return 1;
}

void rect_search_simple(int x1, int y1, int x2, int y2, Point plist[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        if ( (plist[i].x > x1 && plist[i].x < x2) &&
             (plist[i].y > y1 && plist[i].y < y2) ) {
            printf("(%d, %d) \n", plist[i].x, plist[i].y);
        }
    }
}


/* k-d木を作成する
 * stypeはソートする座標(0ならy座標, 1ならx座標でソート)
 */
Treenode *construct_kdtree(Point *g, int start, int end, int stype)
{
    /* (1) start > end ならば何もしない */
    if (start - end > 0) return NULL;
    /* (2) 関数sortxyを使って，g[start]～g[end]をstypeに応じてソート */
    sortxy(g, start, end, stype);
    /* (3) Treenode型の変数領域tを確保し，ソートした範囲の中央値をそこに格納する */
    Treenode *t = (Treenode *)calloc(1, sizeof(Treenode));
    int mid = start + (end - start) / 2;
    t->p.x = g[mid].x;
    t->p.y = g[mid].y;
    /* stypeをトグル */
    if (stype)
        stype = 0;
    else
        stype = 1;
    /* (4),(5) construct_kdtreeを再帰的に呼び出し */
    t->left = construct_kdtree(g, start, mid - 1, stype);
    t->right = construct_kdtree(g, mid + 1, end, stype);

    return t;
}

void rect_search(Treenode *r, int x1, int y1, int x2, int y2, int stype)
{
    /* (1) r がNULLならなにもしない */
    if (r == NULL) return;
    /* (2) 点が範囲に含まれる場合は出力 */
    STATUS status;
    if ( (r->p.x > x1 && r->p.x < x2) &&
        (r->p.y > y1 && r->p.y < y2) ) {
        printf("(%d, %d) \n", r->p.x, r->p.y);
        status = OVER;
    } else if ((stype == 1 && r->p.x > x1 && r->p.x < x2)
               || (stype == 0 && r->p.y > y1 && r->p.y < y2)) {
        /* 点の分割線上にクエリが含まれる場合 */
        status = OVER;
    } else {    /* 範囲に含まれない場合は状態を調べる */
        if (stype == 1) {
            if (x2 < r->p.x) {
                status = LEFT;  /* クエリは左側 */
            } else if (x1 > r->p.x) {
                status = RIGHT; /* クエリは右側 */
            }
        } else if (stype == 0) {
            if (y2 < r->p.y) {
                status = BELOW;  /* クエリは下 */
            } else if (y1 > r->p.y) {
                status = ABOVE; /* クエリは上 */
            }
        }
    }
    /* (3) r->left, r->rightがNULLでないなら，範囲を適切に分割して再帰 */
    /* stypeをトグル */
    if (stype)
        stype = 0;
    else
        stype = 1;
    switch (status) {
        case LEFT:
        case BELOW: /* 左側の子を探索 */
            if (r->left != NULL) {
                rect_search(r->left, x1, y1, x2, y2, stype);
            }
            break;
        case RIGHT:
        case ABOVE: /* 右側の子を探索 */
            if (r->right != NULL) {
                rect_search(r->right, x1, y1, x2, y2, stype);
            }
            break;
        case OVER:  /* 領域を分割して再帰 */
            if (stype == 0) {
                rect_search(r->left, x1, y1, r->p.x, y2, stype);
                rect_search(r->right, r->p.x, y1, x2, y2, stype);
            } else if (stype == 1) {
                rect_search(r->left, x1, y1, x2, r->p.y, stype);
                rect_search(r->right, x1, r->p.y, x2, y2, stype);
            }
        default:
            break;
    }
}


/* クイックソートを用いて，Pointの配列gを，startからendの区間までソートする
 stype = 1 の時はx座標， stype = 0 のときはy座標でソートする */
void sortxy(Point *g, int start, int end, int stype)
{
    int i, j;
    int pivot;
    Point work;

    /* x座標でソートするとき */
    if (stype == 1) {
        pivot = g[ ( start + end ) / 2 ].x;
        i = start;
        j = end;
        while( 1 ) {
            while( g[i].x < pivot ){ ++i; }
            while( g[j].x > pivot ){ --j; }
            if( i >= j ){ break; }
            work = g[i];
            g[i] = g[j];
            g[j] = work;
            i++;
            j--;
        }
        if( start < i - 1 ){ sortxy(g, start, i - 1, stype); }
        if( j + 1 < end ){ sortxy(g, j + 1, end, stype); }
        /* y座標でソートするとき */
    }else {
        pivot = g[ ( start + end ) / 2 ].y;
        i = start;
        j = end;
        while( 1 ) {
            while( g[i].y < pivot ){ ++i; }
            while( g[j].y > pivot ){ --j; }
            if( i >= j ){ break; }
            work = g[i];
            g[i] = g[j];
            g[j] = work;
            i++;
            j--;
        }
        if( start < i - 1 ){ sortxy(g, start, i - 1, stype); }
        if( j + 1 < end ){ sortxy(g, j + 1, end, stype); }
    }
}
