/*------------------------------------------------------------------------------
kdtree.c : Range Query with k-d tree

  Programmed by T.Izumi (2013/01/20)
------------------------------------------------------------------------------*/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 取り扱うデータ数の最大値 */
#define Datasize 100000

typedef struct point {
  int x;
  int y;
} Point;

typedef struct treenode {
  Point p;
  struct treenode *left;
  struct treenode *right;
} Treenode;

void sortxy_sample(Point *g, int start, int end, int stype);
void rect_search_sample(Treenode *r, int x1, int y1, int x2, int y2, int stype);
Treenode *construct_kdtree_sample(Point *g, int start, int end, int stype);


int main_sample(int argc, char *argv[])
{
  FILE *file;
  char firstline[20];
  int x, y;
  int x1, y1, x2, y2;
  int j, size;
  Point plist[Datasize];
  Treenode *root;

  /* ファイルからの情報読み込み 
     第一引数に与えたファイル名を入力として読み込む */
  if ((file = fopen(argv[1],"r")) == NULL){
    printf("unable to open the file!\n");
    exit(1);
  }

  j = 0; size = 0;
  while (fscanf(file,"%d,%d",&x,&y) != EOF) {
    plist[j].x = x;
    plist[j].y = y;
    j++; size ++;
  };
  
  root = construct_kdtree(plist, 0, size - 1, 1);

  fclose(file);

  getchar();
  
  /* 第２引数のファイル名をクエリとして読み出す */
  if ((file = fopen(argv[2],"r")) == NULL){
    printf("unable to open the file!\n");
    exit(1);
  }

  j = 0;
  while (fscanf(file,"%d,%d,%d,%d",&x1,&y1,&x2,&y2) != EOF) {
    printf("query = (%d,%d) - (%d,,%d)\n", x1, y1, x2, y2);
    /* 問い合わせ (x1, y1) - (x2, y2) の範囲 */
    rect_search(root, x1, y1, x2, y2, 1);
    printf("\n");
    j++;
  };

  fclose(file);

  return 1;
}

      

Treenode *construct_kdtree_sample(Point *g, int start, int end, int stype)
{
  /* Point型の配列gについて，g[start] から g[end] の範囲の
    データを格納するk-d木を作成し，その根へのポインタを返す関数を
    ここに作成する．stypeは根の分割方向を表している．
  stype=1のとき根は垂直線(すなわちx座標の大小)で左部分木と
  右部分木に分割され，stype=0のときは水平線(すなわちy座標の大小)で
　分割されているとする．
  関数内の処理は大体以下のようになる．
  (1) start > end ならば何もしない．
  (2) そうでないなら，関数sortxyを使って，g[start]～g[end]を
  stypeに応じた適切な座標値でソートする．
  (3) Treenode型の変数領域tを確保し，
　ソートした範囲の中央値をそこに格納する．
  (4) g[start]～g[mid-1]，の範囲に対してconstruct_kdtreeを再帰的に呼び出し．
  t->leftにその戻り値を格納する．
　(5) g[mid+1]～g[end]に対しても同様にする．

  (4), (5)において，再帰呼び出し時にstypeの値を反転する必要があることに
  注意せよ．
  */
    return NULL;
}

void rect_search_sample(Treenode *r, int x1, int y1, int x2, int y2, int stype)
{
  /* rを根とするk-d木(の部分木)に対して 範囲(x1, y1) - (x2, y2)を
  探索する関数をここに作成する．stypeは根の分割方向を表している．
  stype=1のとき根を中央値として垂直線(すなわちx座標の大小)で左部分木と
  右部分木が分割されていると想定して探索し，stype=0のときは
  水平線(すなわちy座標の大小)で分割されていると想定し探索する．
  関数内でやることは大体以下のようになる．
  (1) r がNULLならなにもしない
  (2) r がNULLでないなら格納されている点が範囲に含まれるか判定し
    　含まれていれば出力
  (3) r->leftがNULLでないなら，範囲を適切に分割してrect_searchを
    　再帰的に呼び出す．
  (4) r->rightがNULLでないなら，範囲を適切に分割してrect_searchを
    　再帰的に呼び出す．
  (3),(4)において，再帰で呼び出すときにstypeの値を反転させる必要がある
　ことはconstruct_kdtreeと同じである．
  */
}


/* クイックソートを用いて，Pointの配列gを，startからendの区間までソートする
  stype = 1 の時はx座標， stype = 0 のときはy座標でソートする */
void sortxy_sample(Point *g, int start, int end, int stype)
{
  int i, j;
  int pivot;   
  Point work;

  /* x座標でソートするとき */
  if (stype == 1) {
    pivot = g[ ( start + end ) / 2 ].x;
    i = start;
    j = end;
    while( 1 ) {
      while( g[i].x < pivot ){ ++i; }
      while( g[j].x > pivot ){ --j; }
      if( i >= j ){ break; }
      work = g[i];
      g[i] = g[j];
      g[j] = work;
      i++;
      j--;
    }
    if( start < i - 1 ){ sortxy(g, start, i - 1, stype); }
    if( j + 1 < end ){ sortxy(g, j + 1, end, stype); }
  /* y座標でソートするとき */
  }else {
    pivot = g[ ( start + end ) / 2 ].y;
    i = start;
    j = end;
    while( 1 ) {
      while( g[i].y < pivot ){ ++i; }
      while( g[j].y > pivot ){ --j; }
      if( i >= j ){ break; }
      work = g[i];
      g[i] = g[j];
      g[j] = work;
      i++;
      j--;
    }
    if( start < i - 1 ){ sortxy(g, start, i - 1, stype); }
    if( j + 1 < end ){ sortxy(g, j + 1, end, stype); }
  }
}




