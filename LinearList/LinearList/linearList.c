//
//  listmaker.c
//  LinearList
//
//  Created by Yuki Taguchi on 2014/10/28.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linearList.h"


/* ノード生成関数 */
Node* node_new(int no, int value, Node *next) {
    Node *ndPtr;
    ndPtr = malloc(sizeof (Node));
    if (ndPtr == NULL) {
        return NULL;      /* 割り当て失敗 */
    } else {
        ndPtr->no = no;
        ndPtr->value = value;
        ndPtr->next = next;
        return ndPtr;
    }
}

/* ファイルオープン関数 */
FILE* open_file(int argc, char *argv[]) {
    if (argv[1] == NULL) {
        printf("%s\n", "Error: No argument!");
        return NULL;
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("%s\n", "Error: Cannot open file!");
        return NULL;
    }
    return fp;
}

Node** make_list(int argc, char *argv[], int pref) {
    /* ファイルオープン */
    FILE *fp;
    if (!(fp = open_file(argc, argv))) {
        puts("ファイル入力失敗!");
        exit(1);
    }

    /* 先頭のメタデータを取得 */
    puts("データ入力中...");
    fscanf(fp, "%d,", &vertex);
    fscanf(fp, "%d,", &edge);
    fscanf(fp, "%d,", &direction);
    fscanf(fp, "%d\n", &value);
    if (pref) {
        fscanf(fp, "%d, ", &start);
        fscanf(fp, "%d\n ", &goal);
        printf("start: %d, goal: %d\n", start, goal);
    } else {
        printf("%d %d %d %d\n", vertex, edge, direction, value);
    }

    /* 隣接行列でデータ作成 */
    int matrix[vertex][vertex];

    /* データ => 隣接行列 */
    int i = 0, j = 0;
    char *token;
    /* \n,を区切りに文字列を抽出 */
    for(i = 0; fgets(str , vertex * 3 , fp) != NULL; i++) {
        token = strtok(str, ",\n" );
        matrix[i][j] = atoi(token);
        for(j = 1; token != NULL; j++) {
            token = strtok(NULL,",");
            if(token != NULL) {
                matrix[i][j] = atoi(token);
            }
        }
        j = 0;
    }
    puts("データ入力完了!");

    /* 先頭リストを初期化 */
    Node **index = (Node**)calloc(vertex, sizeof(Node));
    for (i = 0; i <vertex; i++) {
        index[i] = NULL;
    }
    /* 線形リストを生成 */
    for (i = 0; i < vertex; i++) {
        for (j = 0; j < vertex; j++) {
            if (matrix[i][j] != 0) {
                /* ノードを作成 */
                Node *node = node_new(j, matrix[i][j], NULL);    /* number, value, next */
                /* リストの末尾にノードを追加 */
                if (index[i] == NULL) {
                    index[i] = node;
                    continue;
                }
                Node *next_node = index[i];
                while (next_node->next != NULL) {
                    next_node = next_node->next;
                }
                next_node->next = node;
            }
        }
    }

//    /* 線形リストを表示 */
//    for (i = 0; i < vertex; i++) {
//        if (index[i] == NULL) {
//            printf("[%d] is null \n", i);
//            continue;
//        }
//        Node *next_node = index[i];
//        printf("[%d]: %d, ", i, next_node->no);
//        while (next_node->next != NULL) {
//            next_node = next_node->next;
//            printf("%d, ", next_node->no);
//        }
//        printf("\n");
//    }

    return index;
}

int* get_connected_node(Node **index, int no) {
    /* noノードの隣接ノードを探索 */
    int *nodes = (int *)calloc(vertex, sizeof(int));
    if (index[no] == NULL) {
        nodes[0] = -1;
        return nodes;
    }
    Node *next_node = index[no];
    nodes[0] = next_node->no;
    int i = 0;
    for (i = 0; (next_node->next != NULL); i++) {
        next_node = next_node->next;
        nodes[i+1] = next_node->no;
    }
    /* 配列終了の印 */
    nodes[i+1] = -1;
    return nodes;
}

