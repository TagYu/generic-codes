//
//  listmaker.h
//  LinearList
//
//  Created by Yuki Taguchi on 2014/10/29.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#ifndef LinearList_LinearList_h
#define LinearList_LinearList_h

static char str[2000];
int vertex, edge, direction, value, start, goal;

typedef struct _Node{
    int no; /* 頂点番号 */
    int value;   /* 前のノードとの辺の重み */
    int parent; /* bfsで探索したときの親ノード */
    struct _Node *next; /* 次のノードへのポインタ */
} Node;

Node** make_list(int argc, char *argv[], int pref);

int* get_connected_node(Node **index, int no);
#endif
