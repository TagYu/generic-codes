//
//  main.c
//  LinearList
//
//  Created by Yuki Taguchi on 2014/10/29.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "linearList.h"
#include "queue.h"

/* Queue */
int **path ;
int *length;
const int SIZE = 8000;

int *parent;

int max = 200;
const int pref = 1;

int main(int argc, char *argv[]) {
    search_path(argc, argv);
}

int search_path(int argc, char *argv[]) {
    int i = 0;
    /* 線形リストを生成 */
    Node **index = make_list(argc, argv, pref);

    printf("各ノードを訪問中...");

    /* 頂点の親の対応表 */
    parent = (int *)calloc(vertex, sizeof(int));

    /* キューを作成 */
    Queue queue;
    QueueAlloc(&queue, max);

    /* 幅優先探索 */
    int visited[vertex];    /* 訪問済みリスト */
    for (i = 0; i < vertex; i++) {
        visited[i] = 0;
    }
    visited[start] = 1;
    QueueEnque(&queue, start);
    while (!QueueIsEmpty(&queue)) {
        int current_vertex = start;
        QueueDeque(&queue, &current_vertex);
        printf("\n%d : ", current_vertex);
        /* 隣接しているノードを取得 */
        int *cntd_nodes = get_connected_node(index, current_vertex);
        /* 隣接ノードの個数をカウント */
        int cnt = 0;
        while (cntd_nodes[cnt] != -1) {
            cnt++;
        }
        for (i = 0; i < cnt; i++) {
            int tmp_vertex = cntd_nodes[i];
            if (tmp_vertex == goal) {
                set_parent(tmp_vertex, current_vertex);
                print_path();
            }
            if (visited[tmp_vertex] == 0) { /* 未訪問なら */
                visited[tmp_vertex] = 1;
                set_parent(tmp_vertex, current_vertex);
                printf("%d, ", tmp_vertex);
                QueueEnque(&queue, tmp_vertex);
            }
        }
    }
    printf("Cannot get to %d\n", goal);
    QueueFree(&queue);
    return 0;
}

void set_parent(int current_vertex, int parent_vertex) {
    parent[current_vertex] = parent_vertex;
}


/* 経路の表示 */
void print_path(void) {
    puts("");
    puts("/* ------ 最短経路 ------ */");
    int i, current_vertex = goal;
    printf("goal(%d) <- ", goal);
    for (i = 0; (parent[current_vertex] != start) && (i < vertex); i++) {
        current_vertex = parent[current_vertex];
        printf("%d <- ", current_vertex);
    }
    printf("start(%d)\n", start);
    exit(0);
}

