//
//  main.h
//  LinearList
//
//  Created by Yuki Taguchi on 2014/10/29.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#ifndef LinearList_main_h
#define LinearList_main_h

int search_path(int argc, char *argv[]);

void print_path(void);

void set_parent(int current_vertex, int parent_vertex);

#endif
