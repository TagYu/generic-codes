//
//  main.c
//  Fibonacci
//
//  Created by Yuki Taguchi on 2014/12/25.
//  Copyright (c) 2014年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <time.h>

#define SELECTOR 1

long long *F;

/* 再帰バージョン */
long long Fibonacci_rec(int n)
{
    if (n == 0 || n == 1) {
        F[0] = F[1] = 1;
        return 1;
    }
    F[n] = Fibonacci_rec(n-1) + Fibonacci_rec(n-2);
    return F[n];
}

/* 再帰なしバージョン */
long long Fibonacci(int n)
{
    int i ;
    F[0] = F[1] = 1;
    for (i = 2; i < n; i++)
        F[i] = F[i-1]+F[i-2];
    return F[n];
}



int main(int argc, const char * argv[])
{
    int d;
    int i;
    printf("数値> ");
    scanf("%d", &d);
    F = (long long*)calloc(d, sizeof(long long));

    clock_t start, end;
    start = clock();

    if (SELECTOR)
        Fibonacci(d);
    else
        Fibonacci_rec(d);

    end = clock();
    char *t_path = "/Users/yuki/Dropbox/Document/基礎アルゴリズム/exe4/result_fibo_dp";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- d = %d ---------------\n", d);
    fprintf(t_fp, "開始時刻:%d\n", start );
    fprintf(t_fp, "終了時刻:%d\n", end );
    fprintf(t_fp, "処理時間:%lu[ns]\n", (end - start) );
    fclose(t_fp);

    for (i = 0; i < d; i++) {
        printf("%lld  ", F[i]);
    }

    puts("");
    return 0;
}
