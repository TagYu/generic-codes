/* キューの操作関数定義 */

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

/*--- キューの初期化 ---*/
int QueueAlloc(Queue *q, int max) {

	q->rear = q->front = q->num = 0;
	if ((q->que = calloc(max, sizeof(int))) == NULL) {
		q->max = 0;						/* 配列の確保に失敗 */
		return (-1);
	}
	q->max = max;
	return (0);
}

/*--- キューの後始末 ---*/
void QueueFree(Queue *q) {

	if (q->que != NULL) {
		free(q->que);
		q->max = 0;
		q->num = 0;
		q->front = 0;
		q->rear = 0;
	}
}

/*--- キューにデータをエンキュー ---*/
int QueueEnque(Queue *q, int x) {

	if (q->num == q->max) {		/* キューがいっぱい */
		return (-1);
	} else if (q->num == 0) {	/* キューが空 */
		q->que[q->rear] = x;
	} else {
		q->que[q->rear + 1] = x;		/* キューが空じゃないとき */
		q->rear++;
	}
	if (q->rear == q->max) {		/* rearが配列の終端なら0へ */
		q->rear = 0;
	}
	q->num++;

	return (0);
}


/*--- キューからデータをデキュー ---*/
int QueueDeque(Queue *q, int *x) {

	if (q->num == 0) {			/* キューが空 */
		return (-1);
	} else if (q->num == 1) {
		*x = q->que[q->front];
	} else {
		*x = q->que[q->front];
		q->front++;
	}
	if (q->front == q->max) {			/* frontが配列の終端なら0へ */
		q->front = 0;
	}
	q->num--;

	return (0);
}

/*--- キューの大きさ ---*/
int QueueSize(const Queue *q) {

	return (q->max);
}

/*--- キューに蓄えられているデータ数 ---*/
int QueueNo(const Queue *q) {

	return (q->num);
}

/*--- キューは空か ---*/
int QueueIsEmpty(const Queue *q) {

	return (q->num <= 0);
}

/*--- キューは満杯か ---*/
int QueueIsFull(const Queue *q) {

	return (q->num >= q->max);
}
