//
//  main.c
//  MaxFlow
//
//  Created by Yuki Taguchi on 2015/01/01.
//  Copyright (c) 2015年 Yuki Taguchi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "queue.h"

#define MAX_SIZE 10000
#define NO_PATH 0

FILE* open_file(int argc, char *argv[]);
void construct_residual(int n, int **g, int **f, int **r);
void st_path(int s, int t, int node, int **g, int *p);
void set_parent(int current_vertex, int parent_vertex);
int* get_connected_node(int **g, int node, int no);
void set_path(int *p, int s, int t);

int *parent, node;

int main(int argc, char *argv[])
{
    FILE *fp = open_file(argc, argv);
    char headerline[50];
    int s, t;
    int edge, direction, weight;

    fgets(headerline, 50, fp);
    sscanf(headerline, "%d,%d,%d,%d", &node, &edge, &direction, &weight);
    fgets(headerline, 50, fp);
    sscanf(headerline, "%d,%d", &s, &t);
    
    printf("%d\n", node);
    
    char *buf;
    char *tp;

    int **matrix, **residual, **flow;

    int i, j;
    //配列の領域確保
    buf = (char *)calloc(node * 15, sizeof(char));
    matrix = (int**)calloc(node, sizeof(int *));
    residual = (int**)calloc(node, sizeof(int *));
    flow = (int**)calloc(node, sizeof(int *));
    for (i = 0; i < node; i++){
        matrix[i] = (int*)calloc(node, sizeof(int));
        residual[i] = (int*)calloc(node, sizeof(int));
        flow[i] = (int*)calloc(node, sizeof(int));
    }
    //隣接行列をmatrixに格納
    for (i = 0; i < node; i++){
        fgets(buf, node * 15, fp);
        tp = strtok(buf, ",");
        j = 0;
        while (tp != NULL && j < node) {
            matrix[i][j] = atoi(tp);
            j++;
            tp = strtok(NULL, ",");
        }
    }
    //フローの初期化
    for (i = 0; i < node; i++){
        for (j = 0; j < node; j++){
            flow[i][j] = 0;
        }
    }
    clock_t start, end;
    start = clock();
    /* parent初期化 */
    parent = (int *)calloc(node, sizeof(int));
    /* Ford-Fulkersonアルゴリズム */
    int *p = (int *)calloc(node, sizeof(int));
    while (1) {
        construct_residual(node, matrix, flow, residual);
        st_path(s, t, node, residual, p);
        if (p[0] == -1) {   /* s-tパスが無かったら */
            break;
        }
        int min = residual[p[0]][p[1]];
        for (i = 0; p[i] != t; i++) {    /* 最小カットを求める */
            if (residual[p[i]][p[i+1]] < min) {
                min = residual[p[i]][p[i+1]];
            }
        }
        for (i = 0; p[i] != t; i++) {    /* 通常の辺かどうかに基づいてflowを更新 */
            if (matrix[p[i]][p[i+1]] > 0) {
                flow[p[i]][p[i+1]] += min;
            } else {
                flow[p[i+1]][p[i]] -= min;
            }
        }
    }
    end = clock();
    char *t_path = "/Users/yuki/Dropbox/Document/基礎アルゴリズム/exe5/result.txt";
    FILE *t_fp = fopen(t_path, "a");
    if (t_fp == NULL) {
        printf("%s\n", "Cannot open file!");
        return -1;
    }
    fprintf(t_fp, "-------------- %s ---------------\n", argv[1]);
    fprintf(t_fp, "開始時刻:%ld\n", start );
    fprintf(t_fp, "終了時刻:%ld\n", end );
    fprintf(t_fp, "処理時間:%ld[ms]\n", (end - start) / 1000 );
    fclose(t_fp);

    // 最大フローを表示
    printf("result \n");
    for (i = 0; i < node; i++){
        for (j = 0; j < node; j++){
            printf("%d ", flow[i][j]);
        }
        printf("\n");
    }
}

/*残余グラフの構成
 r[i][j] に頂点 iからjへの残余辺の容量を記録する
 i->jに残余辺が張られないときは明示的にゼロを代入すること
 gは隣接行列， fは現在のフローが渡されているとする
 */
void construct_residual(int node, int **g, int **f, int **r){

    int i, j;
    for (i = 0; i < node; i++) {
        for (j = 0; j < node; j++) {
            if (g[i][j] > 0) {  /* i->jのパイプがあれば */
                r[i][j] = g[i][j] - f[i][j];
            } else if (g[j][i] > 0) { /* i->jの逆向きのパイプがあれば */
                r[i][j] = f[j][i];
            } else {
                r[i][j] = NO_PATH;
            }
        }
    }
}

/* s-tパスの発見
 p[0], p[1], p[2] ...  に発見したパスが通る頂点系列を
 格納すること．もしパスが存在しないときはp[0] = -1 を代入する．
 gには残余グラフを渡すので，g[i][j]の値は0,1ではなく，辺があるとき
 g[i][j] > 0, ないとき g[i][j] == 0 であることに注意
 */
void st_path(int s, int t, int node, int **g, int *p){
    int i = 0;
    /* キューを作成 */
    Queue queue;
    QueueAlloc(&queue, MAX_SIZE);

    /* 幅優先探索 */
    int visited[node];    /* 訪問済みリスト */
    for (i = 0; i < node; i++) {
        visited[i] = 0;
    }
    visited[s] = 1;
    QueueEnque(&queue, s);
    while (!QueueIsEmpty(&queue)) {
        int current_vertex = s;
        QueueDeque(&queue, &current_vertex);
        /* 隣接しているノードを取得 */
        int *cntd_nodes = get_connected_node(g, node, current_vertex);
        /* 隣接ノードの個数をカウント */
        int cnt = 0;
        while (cntd_nodes[cnt] != -1) {
            cnt++;
        }
        for (i = 0; i < cnt; i++) {
            int tmp_vertex = cntd_nodes[i];
            if (tmp_vertex == t) {
                set_parent(tmp_vertex, current_vertex);
                set_path(p, s, t);
                return;
            }
            if (visited[tmp_vertex] == 0) { /* 未訪問なら */
                visited[tmp_vertex] = 1;
                set_parent(tmp_vertex, current_vertex);
                QueueEnque(&queue, tmp_vertex);
            }
        }
    }
    p[0] = -1;
    return;
}

int* get_connected_node(int **g, int node, int s) {
    /* sノードの隣接ノードを探索 */
    int *nodes = (int *)calloc(node, sizeof(int));
    int i, cnt = 0;
    for (i = 0; i < node; i++) {
        if (g[s][i] > 0) {
            nodes[cnt] = i;
            cnt++;
        }
    }
    /* 配列終了の印 */
    nodes[cnt] = -1;
    return nodes;
}

void set_parent(int current_vertex, int parent_vertex) {
    parent[current_vertex] = parent_vertex;
}

/* 経路情報の追加 */
void set_path(int *p, int s, int t) {
    int i, current_vertex = t;
    int *tmp = (int *)calloc(node, sizeof(int));
    for (i = 0; parent[current_vertex] != s; i++) { /* tからsまでの距離を求める */
        tmp[i] = current_vertex;
        current_vertex = parent[current_vertex];
    }
    tmp[i] = current_vertex;

    p[0] = s;
    int j;
    for (j = 0; j < i; j++) {   /* t->sを逆順でコピー */
        p[j+1] = tmp[i-j];
    }
    p[j+1] = t;
    return;
}

/* ------- ファイルオープン関数 ------- */
FILE* open_file(int argc, char *argv[]) {
    if (argv[1] == NULL) {
        printf("%s\n", "Error: No argument!");
        exit(1);
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("%s\n", "Error: Cannot open file!");
        exit(1);
    }
    return fp;
}