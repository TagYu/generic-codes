/*------------------------------------------------------------------------------
 maxflow.c : Solving Max-flow Problem by Ford-Fulkerson's algorithm

 Programmed by T.Izumi (2014/12/31)
 ------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void construct_residual(int n, int **g, int **f, int **r);
void st_path(int s, int t, int node, int **g, int *p);

void main_sample(int argc, char *argv[])
{
    char headerline[50];
    int s, t;
    int node, edge, direction, weight;

    fgets(headerline, 50, stdin);
    sscanf(headerline, "%d,%d,%d,%d", &node, &edge, &direction, &weight);
    fgets(headerline, 50, stdin);
    sscanf(headerline, "%d,%d", &s, &t);

    char *buf;
    char *tp;

    int **matrix, **residual, **flow;

    int i, j;
    //配列の領域確保
    buf = (char *)calloc(node * 15, sizeof(char));
    matrix = (int**)calloc(node, sizeof(int));
    residual = (int**)calloc(node, sizeof(int));
    flow = (int**)calloc(node, sizeof(int));
    for (i = 0; i < node; i++){
        matrix[i] = (int*)calloc(node, sizeof(int));
        residual[i] = (int*)calloc(node, sizeof(int));
        flow[i] = (int*)calloc(node, sizeof(int));
    }
    //隣接行列をmatrixに格納
    for (i = 0; i < node; i++){
        fgets(buf, node * 15, stdin);
        tp = strtok(buf, ",");
        j = 0;
        while (tp != NULL && j < node) {
            matrix[i][j] = atoi(tp);
            j++;
            tp = strtok(NULL, ",");
        }
    }

    //フローの初期化
    for (i = 0; i < node; i++){
        for (j = 0; j < node; j++){
            flow[i][j] = 0;
        }
    }

    /* ここにアルゴリズムを実装する
     このサンプルプログラムでは，残余グラフを構成する関数
     construct_residualを作成してアルゴリズムを実現することを
     想定しているが，この方針に必ず従わないといけないわけでは
     ない．
     */

}




void construct_residual(int node, int **g, int **f, int **r){

    /*残余グラフの構成
     r[i][j] に頂点 iからjへの残余辺の容量を記録する
     i->jに残余辺が張られないときは明示的にゼロを代入すること
     gは隣接行列， fは現在のフローが渡されているとする
     */

    /* おまけ：
     この関数は基本的にrの内容を全部書き換えることを前提としているが，
     実際のところ，フローfの更新による残余グラフの変化は発見した
     s-tパスに添った部分のみで起きる．この関数を，発見したパスpを
     引数として渡し，pに添った部分のみrの値を書き換えるような
     内容に書き換えることで，大幅な高速化を図ることができる．
     (ただしそのように書き換えるとrの初期化にこの関数は使えなくなることに
     注意せよ)．余力のある人はトライしてみるとよい
     */

}

void st_path(int s, int t, int node, int **g, int *p){
    /* s-tパスの発見
     p[0], p[1], p[2] ...  に発見したパスが通る頂点系列を
     格納すること．もしパスが存在しないときはp[0] = -1 を代入する．
     gには残余グラフを渡すので，g[i][j]の値は0,1ではなく，辺があるとき
     g[i][j] > 0, ないとき g[i][j] == 0 であることに注意
     */
    
}
